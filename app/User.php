<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    //    массив для записи
    protected $fillable = [
        'name', 'surname', 'midname', 'service', 'email', 'password',
    ];

    //    массив для скрытой записи
    protected $hidden = [
        'password', 'remember_token',
    ];

    //    массив атрибутов с наативным типом
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
