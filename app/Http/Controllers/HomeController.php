<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    // проверка роли авторизации
    public function __construct()
    {
        $this->middleware('auth');
    }

    // отображение главной страницы
    public function index()
    {
        return view('home');
    }
}
