<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    //    перенаправление на главную страницу
    protected $redirectTo = RouteServiceProvider::HOME;

    //    проверка если гость, то выходим
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
