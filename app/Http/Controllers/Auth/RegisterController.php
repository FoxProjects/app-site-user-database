<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    use RegistersUsers;

    //    перенаправление на главную страницу
    protected $redirectTo = RouteServiceProvider::HOME;

    //    проверка роли гостя
    public function __construct()
    {
        $this->middleware('guest');
    }

    //    валидация данных
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:50'],
            'surname' => ['required', 'string', 'max:50'],
            'midname' => ['required', 'string', 'max:50'],
            'service' => ['required', 'string', 'max: 500'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    //    создание заявки пользователя
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'midname' => $data['midname'],
            'service' => $data['service'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
