<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //    отображение пользователей
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    //    отображение информации о выбранном пользователи
    public function show($id)
    {
        $user = User::find($id);

        return view('admin.users.show', compact('user'));
    }

    //    отображение страницы изменения пользователя
    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.users.edit', compact('user'));
    }

    //    функция валлиодированния данных
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:50'],
            'surname' => ['required', 'string', 'max:50'],
            'midname' => ['required', 'string', 'max:50'],
            'service' => ['required', 'string', 'max: 500'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);
    }

    //    изменение информации пользователя
    public function update(Request $request, $id)
    {
        $this->validator($request->all())->validate();

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->surname = $request->get('surname');
        $user->midname = $request->get('midname');
        $user->service = $request->get('service');
        $user->email = $request->get('email');
        if ( !$request->get('password') == ''){
            $user->password = Hash::make($request->get('password'));
        }
        $user->save();

        return redirect()->route('users.index')->with('status', 'Запись успешно изменена');
    }

    //    удаление выбранного пользователя
    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect()->back()->with('status', 'Запись успешно удалена');
    }
}
