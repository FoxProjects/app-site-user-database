<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    //проверка, является ли пользователей администратором, а так же залогинен ли он
    public function handle($request, Closure $next)
    {

        if(Auth::user() && Auth::user()->is_admin === 1) {
            return $next($request);
        }

        abort(404);
    }
}
