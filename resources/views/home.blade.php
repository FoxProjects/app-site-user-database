@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">{{ __('Главная страница') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>{{ __('Добро пожаловать,') }} {{ Auth::user()->surname }} {{ Auth::user()->name }} {{ Auth::user()->midname }}!</p>
                    <p>{{ __('Ваша почта:') }} {{ Auth::user()->email }}</p>
                    <p>{{ __('Ваш заказ:') }} {{ Auth::user()->service }}</p>
                    <p>{{ __('Ваш пароль:') }} {{ Auth::user()->password }}</p>
                    <p>{{ __('Дата Вашей регистрациия:') }} {{ Auth::user()->created_at }}</p>
                    <p>{{ __('Последние изменения профиля:') }} {{ Auth::user()->updated_at }}</p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
