@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Регистрация') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="form-group row">
                                <div class="col-4">
                                    <input id="name"
                                           type="text"
                                           name="name"
                                           placeholder="Имя"
                                           class="form-control @error('name') is-invalid @enderror"
                                           value="{{ old('name') }}"
                                           required
                                           autocomplete="name"
                                           autofocus
                                    >

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-4">
                                    <input id="midname"
                                           type="text"
                                           name="midname"
                                           placeholder="Отчество"
                                           class="form-control @error('midname') is-invalid @enderror"
                                           value="{{ old('midname') }}"
                                           required
                                           autocomplete="midname"
                                           autofocus
                                    >

                                    @error('midname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-4">
                                    <input id="surname"
                                           type="text"
                                           name="surname"
                                           placeholder="Фамилия"
                                           class="form-control @error('surname') is-invalid @enderror"
                                           value="{{ old('surname') }}"
                                           required
                                           autocomplete="surname"
                                           autofocus
                                    >

                                    @error('surname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <input id="email"
                                           type="email"
                                           name="email"
                                           placeholder="Email"
                                           class="form-control @error('email') is-invalid @enderror"
                                           value="{{ old('email') }}"
                                           required autocomplete="email"
                                    >

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">

                                    <input id="service"
                                           type="text"
                                           name="service"
                                           placeholder="Заявка"
                                           class="form-control @error('service') is-invalid @enderror"
                                           value="{{ old('service') }}"
                                           required autocomplete="service"
                                    >

                                    @error('service')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-12">
                                    <input id="password"
                                           type="password"
                                           name="password"
                                           placeholder="Пароль"
                                           class="form-control @error('password') is-invalid @enderror"
                                           required
                                           autocomplete="new-password"
                                    >

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-12">
                                    <input id="password-confirm"
                                           type="password"
                                           placeholder="Повторите пароль"
                                           class="form-control"
                                           name="password_confirmation"
                                           required
                                           autocomplete="new-password"
                                    >
                                </div>
                            </div>

                            <div class="form-group row mb-0 text-center">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Регистрация') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
