@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">{{ __('Информация пользователе') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <p>{{ __('ФИО:') }} {{ $user->surname }} {{ $user->name }} {{ $user->midname }}</p>
                        <p>{{ __('Заказ:') }} {{ $user->service }}</p>
                        <p>{{ __('Почта:') }} {{ $user->email }}</p>
                        <p>{{ __('Дата регистрациия:') }} {{ $user->created_at }}</p>
                        <p>{{ __('Последние изменения профиля:') }} {{ $user->updated_at }}</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
