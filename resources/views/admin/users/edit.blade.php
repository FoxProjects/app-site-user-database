@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">{{ __('Изменение учетной записи пользователя') }}: {{ $user->name }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <form method="POST" action="{{ route('users.update', $user->id) }}">
                                @method('PATCH')
                                @csrf

                                <div class="form-group row">
                                    <div class="col-4">
                                        <input id="name"
                                               type="text"
                                               name="name"
                                               placeholder="Имя"
                                               class="form-control @error('name') is-invalid @enderror"
                                               value="{{ $user->name }}"
                                               required
                                               autocomplete="name"
                                               autofocus
                                        >

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="col-4">
                                        <input id="midname"
                                               type="text"
                                               name="midname"
                                               placeholder="Отчество"
                                               class="form-control @error('midname') is-invalid @enderror"
                                               value="{{ $user->midname }}"
                                               required
                                               autocomplete="midname"
                                               autofocus
                                        >

                                        @error('midname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="col-4">
                                        <input id="surname"
                                               type="text"
                                               name="surname"
                                               placeholder="Фамилия"
                                               class="form-control @error('surname') is-invalid @enderror"
                                               value="{{ $user->surname }}"
                                               required
                                               autocomplete="surname"
                                               autofocus
                                        >

                                        @error('surname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <input id="email"
                                               type="email"
                                               name="email"
                                               placeholder="Email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               value="{{ $user->email }}"
                                               required autocomplete="email"
                                        >

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <input id="service"
                                               type="text"
                                               name="service"
                                               placeholder="Заявка"
                                               class="form-control @error('service') is-invalid @enderror"
                                               value="{{ $user->service }}"
                                               required
                                        >

                                        @error('service')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-12">
                                        <input id="password"
                                               type="password"
                                               name="password"
                                               placeholder="Пароль"
                                               class="form-control @error('password') is-invalid @enderror"
                                               autocomplete="new-password"
                                        >

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0 text-center">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Изменить') }}
                                        </button>
                                    </div>
                                </div>
                            </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
