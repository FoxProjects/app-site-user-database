@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        @if(count($users) > 0)
                            {{ __('Пользователей:') }} {{ count($users) }}
                        @else
                            {{ __('Пользователей не обнаружено') }}
                        @endif
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="table-responsive-xl">
                            <table id="example" class="table table-sm">

                                <thead>
                                <tr>
                                    <th scope="col">{{ __('#') }}</th>
                                    <th scope="col">{{ __('ФИО') }}</th>
                                    <th scope="col">{{ __('Почта') }}</th>
                                    <th scope="col">{{ __('Роль') }}</th>
                                    <th scope="col" >{{ __('Просмотреть') }}</th>
                                    <th scope="col">{{ __('Изменить') }}</th>
                                    <th scope="col">{{ __('Удалить') }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if(count($users) > 0)
                                    @foreach($users as $user)
                                        <tr>
                                            <th scope="row">{{ $user->id }}</th>
                                            <td>{{ $user->surname }} {{ $user->name }} {{ $user->midname }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if($user->is_admin === 1)
                                                    {{ __('Администратор') }}
                                                @else
                                                    {{ __('Пользователь') }}
                                                @endif
                                            </td>
                                            <td>
                                                <a class="btn btn-block btn-sm btn-primary"
                                                   href="{{ route('users.show', $user->id) }}"
                                                >
                                                    {{ __('Просмотр')}}
                                                </a>
                                            </td>
                                            <td>
                                                @if(!($user->is_admin === 1))
                                                    <a class="btn btn-block btn-sm btn-primary"
                                                       href="{{ route('users.edit', $user->id) }}"
                                                    >
                                                        {{ __('Изменить') }}
                                                    </a>
                                                @endif
                                            </td>
                                            <td>
                                                @if(!(
                                                    (Auth::user()->id === $user->id)
                                                    ||
                                                    ($user->is_admin === 1)
                                                    )
                                                )
                                                    <form method="POST"
                                                          action="{{ route('users.destroy', $user->id) }}"
                                                    >
                                                    @method('DELETE')
                                                    @csrf
                                                        <button class="btn btn-block btn-sm btn-danger"
                                                                type="submit"
                                                        >{{ __('Удалить') }}</button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>Пользователей не существует</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
